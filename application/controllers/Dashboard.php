<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dashboard extends MY_Controller {
	
	public function index()
	{
		$data['title'] = 'Title';
		$this->load_main_html('dashboard');
	}
}
