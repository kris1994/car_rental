

<!DOCTYPE html>
<html lang="en" class="wide wow-animation smoothscroll scrollTo">
  <head>
    <!-- Site Title-->
    <title>Home</title>
    <meta charset="utf-8">
    <meta name="format-detection" content="telephone=no">
    <meta name="viewport" content="width=device-width, height=device-height, initial-scale=1.0, maximum-scale=1.0, user-scalable=0">
    <meta http-equiv="X-UA-Compatible" content="IE=Edge">
    <meta name="keywords" content="intense web design multipurpose template">
    <meta name="date" content="Dec 26">
    <link rel="icon" href="images/favicon.ico" type="image/x-icon">
    <!-- Stylesheets-->
    <link rel="stylesheet" type="text/css" href="//fonts.googleapis.com/css?family=Roboto:400,400italic,700italic,700,300,300italic">
    <link rel="stylesheet" href="css/style.css">
		<!--[if lt IE 10]>
    <div style="background: #212121; padding: 10px 0; box-shadow: 3px 3px 5px 0 rgba(0,0,0,.3); clear: both; text-align:center; position: relative; z-index:1;"><a href="http://windows.microsoft.com/en-US/internet-explorer/"><img src="images/ie8-panel/warning_bar_0000_us.jpg" border="0" height="42" width="820" alt="You are using an outdated browser. For a faster, safer browsing experience, upgrade for free today."></a></div>
    <script src="js/html5shiv.min.js"></script>
		<![endif]-->
	<style data-olark="true" type="text/css">@media print {#habla_beta_container_do_not_rely_on_div_classes_or_names {display: none !important}}</style>
	<link data-olark="true" rel="stylesheet" href="css/theme.css" type="text/css">
  </head>
  <body>
  	<div id="olark" style="display: none;"><olark><iframe frameborder="0" id="olark-loader"></iframe></olark></div>

    <!-- Page-->
    <div class="page text-center">
      <!-- Page Header-->
      <header class="page-head header-panel-absolute bg-gray-darkest slider-menu-position">
        <!-- RD Navbar Transparent-->
        <div class="rd-navbar-wrap">
          <nav data-md-device-layout="rd-navbar-static" data-lg-device-layout="rd-navbar-static" data-stick-up-offset="120" class="rd-navbar rd-navbar-default" data-lg-auto-height="true" data-auto-height="false" data-md-layout="rd-navbar-static" data-lg-layout="rd-navbar-static" data-lg-stick-up="true">
            <div class="rd-navbar-inner">
              <!-- RD Navbar Panel-->
              <div class="rd-navbar-panel">
                <!-- RD Navbar Toggle-->
                <button data-rd-navbar-toggle=".rd-navbar, .rd-navbar-nav-wrap" class="rd-navbar-toggle"><span></span></button>
                <div class="rd-navbar-panel-title veil-md reveal-inline-block">
                  <h4>Home</h4>
                </div>
                <!-- RD Navbar Right Side Toggle-->
                <button data-rd-navbar-toggle=".right-side" class="rd-navbar-right-side-toggle veil-md"><span></span></button>
                <div class="shell">
                  <div class="range range-md-middle">
                    <div class="cell-md-3 left-side">
                      <div class="clearfix text-md-left text-center">
                        <!--Navbar Brand-->
                        <div class="rd-navbar-brand"><a href="index.html"><img width='229' height='48' src='images/logo-light-229x48.png' alt=''/></a></div>
                      </div>
                    </div>
                    <div class="cell-md-9 text-md-right right-side">
                      <ul class="list-unstyled">
                        <li class="reveal-md-inline-block"><span class="icon icon-xxs mdi mdi-email text-middle"></span><a href="mailto:#" class="inset-left-10 text-middle text-light">info@demolink.org</a></li>
                        <li class="reveal-md-inline-block"><span class="icon icon-xxs mdi mdi-clock text-middle"></span><span class="inset-left-10 text-middle text-light">Mon–Sat: 7:00–19:00</span></li>
                        <li class="reveal-md-inline-block"><span class="icon icon-xxs mdi mdi-phone text-middle"></span><a href="callto:#" class="inset-left-10 text-middle text-light">1-800-1234-567</a></li>
                      </ul>
                    </div>
                  </div>
                </div>
              </div>
              <div class="rd-navbar-menu-wrap">
                <div class="shell">
                  <div class="rd-navbar-nav-wrap">
                    <div class="rd-navbar-mobile-scroll">
                      <div class="rd-navbar-mobile-header-wrap">
                        <!--Navbar Brand Mobile-->
                        <div class="rd-navbar-mobile-brand"><a href="index.html"><img width='229' height='48' src='images/logo-light-229x48.png' alt=''/></a><a data-custom-toggle="rd-navbar-search-mobile" href="#" class="rd-navbar-mobile-search-toggle mdi"><span></span></a></div>
                        <!--RD Navbar Mobile Search-->
                        <div id="rd-navbar-search-mobile" class="rd-navbar-search-mobile">
                          <form action="search-results.html" method="GET" class="rd-navbar-search-form search-form-icon-right rd-search">
                            <div class="form-group">
                              <label for="rd-navbar-mobile-search-form-input" class="form-label">Search</label>
                              <input id="rd-navbar-mobile-search-form-input" type="text" name="s" autocomplete="off" class="rd-navbar-search-form-input form-control form-control-gray-lightest"/>
                            </div>
                          </form>
                        </div>
                      </div>
                      <!-- RD Navbar Nav-->
                      <ul class="rd-navbar-nav">
                        <li class=" active"><a href="index.html">Home</a>
                        </li>
                        <li><a href="gallery-masonry.html">Vehicle Gallery</a>
                          <ul class="rd-navbar-dropdown">
                            <li><a href="gallery-masonry.html">Masonry Grid</a>
                            </li>
                            <li><a href="gallery-grid-3-columns.html">Grid With Padding</a>
                            </li>
                            <li><a href="gallery-grid-3-columns-fullwidth.html">Grid With No Padding</a>
                            </li>
                            <li><a href="gallery-cobbles.html">Cobbles Grid</a>
                            </li>
                          </ul>
                        </li>
                        <li class=" tabs-nav"><a href="services.html">Services</a>
                          <ul class="rd-navbar-dropdown">
                            <li><a href="services.html#0">History Check</a>
                            </li>
                            <li><a href="services.html#1">Buyers Guide</a>
                            </li>
                            <li><a href="services.html#2">Car Check</a>
                            </li>
                            <li><a href="services.html#3">Car Insurance</a>
                            </li>
                            <li><a href="services.html#4">Customer Support</a>
                            </li>
                            <li><a href="services.html#5">Warranty Programs</a>
                            </li>
                          </ul>
                        </li>
                        <li><a href="#">Pages</a>
                          <div class="rd-navbar-megamenu">
                            <div class="row section-relative">
                              <div class="col-lg-3 veil reveal-lg-block">
                                <div>
                                  <div>
                                    <p style="min-height:300px"></p>
                                  </div>
                                </div>
                              </div>
                              <ul class="col-lg-3 col-md-4">
                                <li>
                                  <h6 class="text-bold text-white">Pages</h6>
                                  <ul class="list-unstyled offset-lg-top-25">
                                    <li><a href="about-us.html">About Us</a></li>
                                    <li><a href="prices.html">Prices</a></li>
                                    <li><a href="calculate.html">Calculate</a></li>
                                    <li><a href="team.html">Our Team</a></li>
                                    <li><a href="services-1.html">Services 1</a></li>
                                    <li><a href="services-2.html">Services 2</a></li>
                                    <li><a href="team-member.html">Team Member Profile</a></li>
                                    <li><a href="404.html">404</a></li>
                                    <li><a href="maintenance.html">Maintenance</a></li>
                                    <li><a href="comming-soon.html">Coming Soon</a></li>
                                    <li><a href="faq.html">FAQs</a></li>
                                    <li><a href="clients.html">Clients</a></li>
                                  </ul>
                                </li>
                              </ul>
                              <ul class="col-lg-3 col-md-4">
                                <li>
                                  <h6 class="text-bold text-white">Additional Pages</h6>
                                  <ul class="list-unstyled offset-lg-top-25">
                                    <li><a href="tabs-and-accordions.html">Tabs &amp; Accordions</a></li>
                                    <li><a href="typography.html">Typography</a></li>
                                    <li><a href="forms.html">Forms</a></li>
                                    <li><a href="buttons.html">Buttons</a></li>
                                    <li><a href="grid.html">Grid</a></li>
                                    <li><a href="icons.html">Icons</a></li>
                                    <li><a href="tables.html">Tables</a></li>
                                    <li><a href="progress-bars.html">Progress bars</a></li>
                                  </ul>
                                </li>
                              </ul>
                              <ul class="col-lg-3 col-md-4">
                                <li>
                                  <h6 class="text-bold text-white">Elements</h6>
                                  <ul class="list-unstyled offset-lg-top-25">
                                    <li><a href="header-transparent.html">Header Transparent</a></li>
                                    <li><a href="header-center-footer-light.html">Header Center, Footer Light</a></li>
                                    <li><a href="header-minimal-footer-corporate.html">Header Minimal, Footer Corporate</a></li>
                                    <li><a href="header-corporate.html">Header Corporate</a></li>
                                    <li><a href="header-hamburger-menu.html">Header Hamburger Menu</a></li>
                                    <li><a href="footer-center-dark.html">Footer Center Dark</a></li>
                                    <li><a href="footer-center-light.html">Footer Center Light</a></li>
                                    <li><a href="footer-minimal-dark.html">Footer Minimal Dark</a></li>
                                    <li><a href="footer-widget-light.html">Footer Widget Light</a></li>
                                    <li><a href="footer-widget-dark.html">Footer Widget Dark</a></li>
                                  </ul>
                                </li>
                              </ul>
                            </div>
                          </div>
                        </li>
                        <li><a href="blog-default.html">Blog</a>
                          <ul class="rd-navbar-dropdown">
                            <li><a href="blog-archive.html">Archive Page</a>
                            </li>
                            <li><a href="blog-2-columns-layout.html">2 Columns Layout</a>
                            </li>
                            <li><a href="blog-3-column-layout.html">3 Columns Grid Layout</a>
                            </li>
                            <li><a href="blog-default.html">Default Blog</a>
                            </li>
                            <li><a href="blog-left-sidebar.html">Left Sidebar</a>
                            </li>
                            <li><a href="blog-right-sidebar.html">Right Sidebar</a>
                            </li>
                            <li><a href="blog-timeline.html">Timeline</a>
                            </li>
                            <li><a href="blog-post-page.html">Post Page</a>
                            </li>
                          </ul>
                        </li>
                        <li><a href="contacts.html">Contacts</a>
                          <ul class="rd-navbar-dropdown">
                            <li><a href="contacts.html">Contacts</a>
                            </li>
                            <li><a href="contacts-variant-2.html">Contacts 2</a>
                            </li>
                          </ul>
                        </li>
                      </ul>
                    </div>
                  </div>
                  <!--RD Navbar Search-->
                  <div class="rd-navbar-search"><a data-rd-navbar-toggle=".rd-navbar-search" href="#" class="rd-navbar-search-toggle mdi"><span></span></a>
                    <form action="search-results.html" data-search-live="rd-search-results-live" method="GET" class="rd-navbar-search-form search-form-icon-right rd-search">
                      <div class="form-group">
                        <label for="rd-navbar-search-form-input" class="form-label">Search</label>
                        <input id="rd-navbar-search-form-input" type="text" name="s" autocomplete="off" class="rd-navbar-search-form-input form-control form-control-gray-lightest"/>
                        <div id="rd-search-results-live" class="rd-search-results-live"></div>
                      </div>
                    </form>
                  </div>
                </div>
              </div>
            </div>
          </nav>
        </div>
      </header>
      <!-- Page Content-->
      <main class="page-content">
        <section class="context-dark bg-dark-blue">
          <!-- Swiper-->
          <div data-height="100vh" data-loop="true" data-dragable="false" data-min-height="480px" data-slide-effect="true" class="swiper-container swiper-slider">
            <div class="swiper-wrapper">
              <div data-slide-bg="images/services/bg-01-1920x654.jpg" style="background-position: center center" class="swiper-slide">
                <div class="swiper-slide-caption section-md-top-185">
                  <div class="container">
                    <div class="range range-xs-center range-lg-left">
                      <div class="cell-lg-7 text-lg-left cell-xs-10">
                        <div data-caption-animate="fadeInUp" data-caption-delay="100">
                          <h1>New and Used Cars</h1>
                        </div>
                        <div data-caption-animate="fadeInUp" data-caption-delay="150" class="offset-top-15 offset-xl-top-30">
                          <h4 class="font-default text-light text-spacing-20">Explore the vast model range of new and used cars by widely known manufacturers on our website.</h4>
                        </div>
                        <div data-caption-animate="fadeInUp" data-caption-delay="200" class="offset-top-20"><a href="about-us.html" class="btn btn-default btn-sm">learn more</a></div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <div data-slide-bg="images/services/bg-03-1920x766.jpg" style="background-position: center center" class="swiper-slide">
                <div class="swiper-slide-caption section-md-top-185">
                  <div class="container">
                    <div class="range range-xs-center range-lg-left">
                      <div class="cell-lg-7 text-lg-left cell-xs-10">
                        <div data-caption-animate="fadeInUp" data-caption-delay="100">
                          <h1>Latest Car Reviews</h1>
                        </div>
                        <div data-caption-animate="fadeInUp" data-caption-delay="150" class="offset-top-15 offset-xl-top-30">
                          <h4 class="font-default text-light text-spacing-20">Read the latest car reviews written by our consultants or submit your own car review to our website's blog!</h4>
                        </div>
                        <div data-caption-animate="fadeInUp" data-caption-delay="200" class="offset-top-20"><a href="about-us.html" class="btn btn-default btn-sm">learn more</a></div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <div data-slide-bg="images/services/bg-04-1920x766.jpg" style="background-position: center center" class="swiper-slide">
                <div class="swiper-slide-caption section-md-top-185">
                  <div class="container">
                    <div class="range range-xs-center range-lg-left">
                      <div class="cell-lg-7 text-lg-left cell-xs-10">
                        <div data-caption-animate="fadeInUp" data-caption-delay="200">
                          <h1>Locate a Car Dealer</h1>
                        </div>
                        <div data-caption-animate="fadeInUp" data-caption-delay="300" class="offset-top-15 offset-xl-top-30">
                          <h4 class="font-default text-light text-spacing-20">We can help you find an appropriate car dealership according to your preferences and location.</h4>
                        </div>
                        <div data-caption-animate="fadeInUp" data-caption-delay="500" class="offset-top-20"><a href="about-us.html" class="btn btn-default btn-sm"> learn more</a></div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <!-- Swiper Pagination-->
            <div class="swiper-pagination"> </div>
          </div>
        </section>
        <section class="section-70 section-md-90">
          <div class="shell text-sm-left">
            <h2>Car search</h2>
            <hr class="divider divider-lg bg-primary hr-sm-left-0">
            <div class="offset-top-50">
              <p>Please complete and submit this form and one of our representatives will reply promptly. Thank You!</p>
            </div>
            <div class="offset-top-20">
              <form method="post" action="bat/rd-mailform.php" data-form-output="form-output-global" class="rd-mailform text-left">
                <div class="range range-xs-bottom">
                  <div class="cell-sm-6 cell-lg-4">
                    <div class="form-group">
                      <label for="form-car-search-make" class="form-label form-label-outside">Make:</label>
                      <select id="form-car-search-make" name="make" class="form-control">
                        <option value="1">Enter car make...</option>
                        <option value="2">Audi</option>
                        <option value="3">Acura</option>
                        <option value="4">BMW</option>
                        <option value="5">Chevrolet</option>
                      </select>
                    </div>
                  </div>
                  <div class="cell-sm-6 cell-lg-4 offset-top-30 offset-sm-top-0">
                    <div class="form-group">
                      <label for="form-car-search-model" class="form-label form-label-outside">Model:</label>
                      <select id="form-car-search-model" name="model" class="form-control">
                        <option value="1">Enter model</option>
                        <option value="2">x2 series</option>
                        <option value="3">x3 series</option>
                        <option value="4">x4 series</option>
                        <option value="5">x5 series</option>
                      </select>
                    </div>
                  </div>
                  <div class="cell-sm-6 cell-lg-4 offset-top-30">
                    <div class="form-group">
                      <label for="form-car-search-country" class="form-label form-label-outside">Country:</label>
                      <select id="form-car-search-country" name="country" class="form-control">
                        <option value="1">Country</option>
                        <option value="2">USA</option>
                        <option value="3">Germany</option>
                        <option value="4">Italy</option>
                        <option value="5">Japan</option>
                      </select>
                    </div>
                  </div>
                  <div class="cell-sm-6 cell-lg-4 offset-top-30">
                    <div class="form-group">
                      <label for="form-car-search-city" class="form-label form-label-outside">City</label>
                      <select id="form-car-search-city" name="city" class="form-control">
                        <option value="1">City name</option>
                        <option value="2">Tokio</option>
                        <option value="3">Munchen</option>
                        <option value="4">New York</option>
                        <option value="5">Milan</option>
                      </select>
                    </div>
                  </div>
                  <div class="cell-sm-3 cell-lg-4 offset-top-30">
                    <div class="form-group">
                      <label for="form-car-search-body-type" class="form-label form-label-outside">Body Type:</label>
                      <select id="form-car-search-body-type" name="body-type" class="form-control">
                        <option value="1">Enter</option>
                        <option value="2">Hatchback</option>
                        <option value="3">Mini-MPVs</option>
                        <option value="4">Crossovers</option>
                        <option value="5">Coupes</option>
                      </select>
                    </div>
                  </div>
                  <div class="cell-sm-3 cell-lg-2 offset-top-30">
                    <div class="form-group">
                      <label for="form-car-search-min-year" class="form-label form-label-outside"> Min Year:</label>
                      <input id="form-car-search-min-year" type="text" name="min-year" placeholder="0" data-constraints="@Required @IsNumeric" class="form-control">
                    </div>
                  </div>
                  <div class="cell-sm-3 cell-lg-2 offset-top-30">
                    <div class="form-group">
                      <label for="form-car-search-max-year" class="form-label form-label-outside"> Max Year:</label>
                      <input id="form-car-search-max-year" type="text" name="max-year" placeholder="0" data-constraints="@Required @IsNumeric" class="form-control">
                    </div>
                  </div>
                  <div class="cell-sm-3 cell-lg-3 offset-top-30">
                    <div class="form-group">
                      <label for="form-quote-color" class="form-label form-label-outside">Color</label>
                      <select id="form-quote-color" name="color" class="form-control">
                        <option value="1">No</option>
                        <option value="2">Black</option>
                        <option value="3">White</option>
                        <option value="4">Red</option>
                        <option value="5">Blue</option>
                      </select>
                    </div>
                  </div>
                  <div class="cell-sm-3 cell-lg-2 offset-top-30">
                    <div class="form-group">
                      <label for="form-quote-height" class="form-label form-label-outside"> Min Price:</label>
                      <input id="form-quote-height" type="text" name="height" placeholder="0" data-constraints="@Required @IsNumeric" class="form-control">
                    </div>
                  </div>
                  <div class="cell-sm-3 cell-lg-2 offset-top-30">
                    <div class="form-group">
                      <label for="form-quote-width" class="form-label form-label-outside"> Max Price:</label>
                      <input id="form-quote-width" type="text" name="width" placeholder="1" data-constraints="@Required @IsNumeric" class="form-control">
                    </div>
                  </div>
                  <div class="cell-sm-3 cell-lg-2 offset-top-30"><a href="calculate.html" class="btn btn-primary btn-sm">calculate</a></div>
                </div>
              </form>
            </div>
          </div>
        </section>
        <!-- counters-->
        <section class="bg-dark-blue context-dark">
          <!-- RD Parallax-->
          <div data-on="false" data-md-on="true" class="rd-parallax">
            <div data-speed="0.35" data-type="media" data-url="images/services/bg-04-1920x500.jpg" class="rd-parallax-layer"></div>
            <div data-speed="0" data-type="html" class="rd-parallax-layer">
              <div class="shell text-sm-left section-65 section-sm-95">
                <h2>counters</h2>
                <hr class="divider divider-lg bg-primary hr-sm-left-0">
                <div class="range range-xs-center range-md-left offset-top-50 text-center counters">
                  <div class="cell-sm-6 cell-md-3">
                    <!-- Counter type 1-->
                    <div class="counter-type-1">
                      <div>
                        <div class="h2 font-accent text-regular"><span data-step="3000" data-from="0" data-to="1324" class="counter"></span></div>
                      </div>
                      <div class="offset-top-12">
                        <h6 class="text-primary text-regular">Types of models</h6>
                      </div>
                    </div>
                  </div>
                  <div class="cell-sm-6 cell-md-3 offset-top-65 offset-sm-top-0">
                    <!-- Counter type 1-->
                    <div class="counter-type-1">
                      <div>
                        <div class="h2 font-accent text-regular"><span data-step="2500" data-from="0" data-to="65" class="counter"></span></div>
                      </div>
                      <div class="offset-top-12">
                        <h6 class="text-primary text-regular">Certified consultants</h6>
                      </div>
                    </div>
                  </div>
                  <div class="cell-sm-6 cell-md-3 offset-top-65 offset-md-top-0">
                    <!-- Counter type 1-->
                    <div class="counter-type-1">
                      <div>
                        <div class="h2 font-accent text-regular"><span data-step="1500" data-from="0" data-to="268" class="counter"></span></div>
                      </div>
                      <div class="offset-top-12">
                        <h6 class="text-primary text-regular">Models we sell</h6>
                      </div>
                    </div>
                  </div>
                  <div class="cell-sm-6 cell-md-3 offset-top-65 offset-md-top-0">
                    <!-- Counter type 1-->
                    <div class="counter-type-1">
                      <div>
                        <div class="h2 font-accent text-regular"><span data-step="5300" data-from="0" data-to="2847951" class="counter"></span></div>
                      </div>
                      <div class="offset-top-12">
                        <h6 class="text-primary text-regular">Happy clients</h6>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </section>
        <!-- Classic Thumbnail-->
        <section class="section-80 section-lg-90 bg-lightest">
          <div class="shell text-sm-left">
            <h2>our services</h2>
            <hr class="divider divider-lg bg-primary hr-sm-left-0">
            <div class="range range-xs-center range-sm-left offset-top-50">
              <div class="cell-xs-10 cell-sm-8">
                <p>We strive to provide our customers with the best services possible.</p>
              </div>
            </div>
            <div class="range text-left range-xs-center offset-top-50">
              <div class="cell-xs-8 cell-sm-6 cell-md-4"><a class="thumbnail-classic" href="services.html" target="_self">
                          <figure><img width="370" height="250" src="images/services/service-01-370x250.jpg" alt="">
                            <figcaption class="thumbnail-classic-caption"><span class="icon icon-xxs fa-arrow-right"></span>
                              <h6 class="thumbnail-classic-title offset-top-0 text-uppercase">History Check</h6>
                            </figcaption>
                          </figure></a>
              </div>
              <div class="cell-xs-8 cell-sm-6 cell-md-4 offset-top-30 offset-sm-top-0"><a class="thumbnail-classic" href="services.html#undefined2" target="_self">
                          <figure><img width="370" height="250" src="images/services/service-02-370x250.jpg" alt="">
                            <figcaption class="thumbnail-classic-caption"><span class="icon icon-xxs fa-arrow-right"></span>
                              <h6 class="thumbnail-classic-title offset-top-0 text-uppercase">Buyers Guide</h6>
                            </figcaption>
                          </figure></a>
              </div>
              <div class="cell-xs-8 cell-sm-6 cell-md-4 offset-top-30 offset-md-top-0"><a class="thumbnail-classic" href="services.html#undefined3" target="_self">
                          <figure><img width="370" height="250" src="images/services/service-03-370x250.jpg" alt="">
                            <figcaption class="thumbnail-classic-caption"><span class="icon icon-xxs fa-arrow-right"></span>
                              <h6 class="thumbnail-classic-title offset-top-0 text-uppercase">Car Insurance</h6>
                            </figcaption>
                          </figure></a>
              </div>
              <div class="cell-xs-8 cell-sm-6 cell-md-4 offset-top-30"><a class="thumbnail-classic" href="services.html#undefined4" target="_self">
                          <figure><img width="370" height="250" src="images/services/service-04-370x250.jpg" alt="">
                            <figcaption class="thumbnail-classic-caption"><span class="icon icon-xxs fa-arrow-right"></span>
                              <h6 class="thumbnail-classic-title offset-top-0 text-uppercase">Car Check</h6>
                            </figcaption>
                          </figure></a>
              </div>
              <div class="cell-xs-8 cell-sm-6 cell-md-4 offset-top-30"><a class="thumbnail-classic" href="services.html#undefined5" target="_self">
                          <figure><img width="370" height="250" src="images/services/service-05-370x250.jpg" alt="">
                            <figcaption class="thumbnail-classic-caption"><span class="icon icon-xxs fa-arrow-right"></span>
                              <h6 class="thumbnail-classic-title offset-top-0 text-uppercase">Warranty Programs</h6>
                            </figcaption>
                          </figure></a>
              </div>
              <div class="cell-xs-8 cell-sm-6 cell-md-4 offset-top-30"><a class="thumbnail-classic" href="services.html#undefined6" target="_self">
                          <figure><img width="370" height="250" src="images/services/service-06-370x250.jpg" alt="">
                            <figcaption class="thumbnail-classic-caption"><span class="icon icon-xxs fa-arrow-right"></span>
                              <h6 class="thumbnail-classic-title offset-top-0 text-uppercase">Customer Support</h6>
                            </figcaption>
                          </figure></a>
              </div>
            </div>
            <div class="offset-top-50"><a href="services-1.html" class="btn btn-default btn-sm">view all services</a></div>
          </div>
        </section>
        <section class="section-80">
          <div class="shell text-sm-left">
            <h2>Professional team</h2>
            <hr class="divider divider-lg bg-primary hr-sm-left-0">
            <div class="range range-xs-center range-sm-left offset-top-50">
              <div class="cell-xs-10 cell-sm-6 cell-md-3"><img src="images/services/user-alan-smith-270x270.jpg" width="270" height="270" alt="" class="img-responsive reveal-inline-block">
                <div class="offset-top-20">
                  <h5 class="text-primary text-regular"><a href="team-member.html">Alan Smith</a></h5>
                </div>
                <p class="offset-top-5 text-silver text-light">Owner/Partner</p>
                <p>Alan’s rich experience includes car sales, rental car management, and service advising.</p>
                <div class="offset-top-20">
                  <ul class="list-inline list-inline-dark list-inline-sm">
                    <li><a href="#" class="icon icon-xxs fa-facebook text-middle"></a></li>
                    <li><a href="#" class="icon icon-xxs fa-twitter text-middle"></a></li>
                    <li><a href="#" class="icon icon-xxs fa-google text-middle"></a></li>
                  </ul>
                </div>
              </div>
              <div class="cell-xs-10 cell-sm-6 cell-md-3 offset-top-50 offset-sm-top-0"><img src="images/services/user-laura-stegner-270x270.jpg" width="270" height="270" alt="" class="img-responsive reveal-inline-block">
                <div class="offset-top-20">
                  <h5 class="text-primary text-regular"><a href="team-member.html">Laura Stegner</a></h5>
                </div>
                <p class="offset-top-5 text-silver text-light">Sales Manager</p>
                <p>Her goal is to help you make a wise investment decision regarding your needs.</p>
                <div class="offset-top-20">
                  <ul class="list-inline list-inline-dark list-inline-sm">
                    <li><a href="#" class="icon icon-xxs fa-facebook text-middle"></a></li>
                    <li><a href="#" class="icon icon-xxs fa-twitter text-middle"></a></li>
                    <li><a href="#" class="icon icon-xxs fa-google text-middle"></a></li>
                  </ul>
                </div>
              </div>
              <div class="cell-xs-10 cell-sm-6 cell-md-3 offset-top-50 offset-md-top-0"><img src="images/services/user-john-franklin-270x270.jpg" width="270" height="270" alt="" class="img-responsive reveal-inline-block">
                <div class="offset-top-20">
                  <h5 class="text-primary text-regular"><a href="team-member.html">John Franklin</a></h5>
                </div>
                <p class="offset-top-5 text-silver text-light">Certified Technician</p>
                <p>Everyone enjoys his expertise as a technician and a great personality around the shop.</p>
                <div class="offset-top-20">
                  <ul class="list-inline list-inline-dark list-inline-sm">
                    <li><a href="#" class="icon icon-xxs fa-facebook text-middle"></a></li>
                    <li><a href="#" class="icon icon-xxs fa-twitter text-middle"></a></li>
                    <li><a href="#" class="icon icon-xxs fa-google text-middle"></a></li>
                  </ul>
                </div>
              </div>
              <div class="cell-xs-10 cell-sm-6 cell-md-3 offset-top-50 offset-md-top-0"><img src="images/services/user-martha-healy-270x270.jpg" width="270" height="270" alt="" class="img-responsive reveal-inline-block">
                <div class="offset-top-20">
                  <h5 class="text-primary text-regular"><a href="team-member.html">Martha Healy</a></h5>
                </div>
                <p class="offset-top-5 text-silver text-light">Service Advisor</p>
                <p>Martha is a leader in providing each of our customers with the excellent customer service.</p>
                <div class="offset-top-20">
                  <ul class="list-inline list-inline-dark list-inline-sm">
                    <li><a href="#" class="icon icon-xxs fa-facebook text-middle"></a></li>
                    <li><a href="#" class="icon icon-xxs fa-twitter text-middle"></a></li>
                    <li><a href="#" class="icon icon-xxs fa-google text-middle"></a></li>
                  </ul>
                </div>
              </div>
            </div>
          </div>
        </section>
        <!-- Testimonials-->
        <section class="bg-dark-blue context-dark">
          <!-- RD Parallax-->
          <div data-on="false" data-md-on="true" class="rd-parallax">
            <div data-speed="0.35" data-type="media" data-url="images/services/bg-05-1920x609.jpg" class="rd-parallax-layer"></div>
            <div data-speed="0" data-type="html" class="rd-parallax-layer">
              <div class="shell text-md-left section-90 section-md-bottom-100">
                <h2>Testimonials</h2>
                <hr class="divider divider-lg bg-primary hr-md-left-0">
                <div class="offset-top-50">
                  <div data-loop="false" data-items="1" data-dots="true" data-mouse-drag="false" data-sm-items="2" data-nav="false" data-margin="30" class="owl-carousel owl-carousel-default">
                    <div>
                      <div class="quote">
                        <div class="unit unit-md unit-md-horizontal text-md-left">
                          <div class="unit-left"><img src="images/services/user-evelyn-fisher-140x140.jpg" width="140" height="140" alt="" class="img-responsive reveal-inline-block img-circle"></div>
                          <div class="unit-body">
                            <div class="inset-md-right-40">
                              <p>
                                <q>I had a great experience at this car dealership. Eva was quick, and she helped us find the car and price we hoped for. We had a hard time finding car we wanted but you had a perfect match for us!</q>
                              </p>
                              <p>
                                <cite>Evelyn Fisher</cite>
                              </p>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                    <div>
                      <div class="quote">
                        <div class="unit unit-md unit-md-horizontal text-md-left">
                          <div class="unit-left"><img src="images/services/user-lawrence-sims-140x140.jpg" width="140" height="140" alt="" class="img-responsive reveal-inline-block img-circle"></div>
                          <div class="unit-body">
                            <div class="inset-md-right-40">
                              <p>
                                <q>I have never thought purchasing a car can be so easy! You not just offer wide assortment of cars and vehicles, you provide pleasant customer service, and it’s what I like most!</q>
                              </p>
                              <p>
                                <cite>Lawrence Sims</cite>
                              </p>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                    <div>
                      <div class="quote">
                        <div class="unit unit-md unit-md-horizontal text-md-left">
                          <div class="unit-left"><img src="images/services/user-jenny-myers-140x140.jpg" width="140" height="140" alt="" class="img-responsive reveal-inline-block img-circle"></div>
                          <div class="unit-body">
                            <div class="inset-md-right-40">
                              <p>
                                <q>These guys are true professionals. Not only they gave me a great value for my old car but also helped me find an amazing new vehicle. They have a very nice selection of  affordable vehicles.</q>
                              </p>
                              <p>
                                <cite>Cristal Smith</cite>
                              </p>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                    <div>
                      <div class="quote">
                        <div class="unit unit-md unit-md-horizontal text-md-left">
                          <div class="unit-left"><img src="images/services/user-peter-johnson-140x140.jpg" width="140" height="140" alt="" class="img-responsive reveal-inline-block img-circle"></div>
                          <div class="unit-body">
                            <div class="inset-md-right-40">
                              <p>
                                <q>I had a great experience at this car dealership. Eva was quick, and she helped us find the car and price we hoped for. We had a hard time finding car we wanted but you had a perfect match for us!</q>
                              </p>
                              <p>
                                <cite>Patrick Pool</cite>
                              </p>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                    <div>
                      <div class="quote">
                        <div class="unit unit-md unit-md-horizontal text-md-left">
                          <div class="unit-left"><img src="images/services/user-lawrence-sims-140x140.jpg" width="140" height="140" alt="" class="img-responsive reveal-inline-block img-circle"></div>
                          <div class="unit-body">
                            <div class="inset-md-right-40">
                              <p>
                                <q>Frankly, I have bought a few vehicles from different dealerships, but AutoTraider has the most professional sales managers and they treat customers with respect. I will surely recommend them to my friends.</q>
                              </p>
                              <p>
                                <cite>Sam Kromstain</cite>
                              </p>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                    <div>
                      <div class="quote">
                        <div class="unit unit-md unit-md-horizontal text-md-left">
                          <div class="unit-left"><img src="images/services/user-evelyn-fisher-140x140.jpg" width="140" height="140" alt="" class="img-responsive reveal-inline-block img-circle"></div>
                          <div class="unit-body">
                            <div class="inset-md-right-40">
                              <p>
                                <q>My husband and I had a really good experience. Never felt pressured, haggled, or lied to. They gave us fair market value without having to negotiate. Also, this dealership is very nicely updated.</q>
                              </p>
                              <p>
                                <cite>Eva Pool</cite>
                              </p>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </section>
      </main>
      <!-- Page Footer-->
      <footer class="section-90 section-bottom-50 page-footer bg-gray-darkest">
        <div class="shell">
          <!--Footer brand--><a href="index.html"><img width='177' height='78' src='images/brand-footer-177x78.png' alt=''/></a>
          <div class="offset-top-20">
            <ul class="list-inline list-inline-dark">
              <li><a href="#" class="icon icon-xxs fa-facebook"></a></li>
              <li><a href="#" class="icon icon-xxs fa-twitter"></a></li>
              <li><a href="#" class="icon icon-xxs fa-pinterest"></a></li>
              <li><a href="#" class="icon icon-xxs fa-vimeo"></a></li>
              <li><a href="#" class="icon icon-xxs fa-google"></a></li>
              <li><a href="#" class="icon icon-xxs fa-rss"></a></li>
            </ul>
          </div>
          <div class="offset-top-20">
            <!-- {%FOOTER_LINK}-->
            <p class="text-gray">&copy; <span id="copyright-year"></span> All Rights Reserved Terms of Use and <a href="privacy.html" class="text-dark">Privacy Policy</a></p>
          </div>
        </div>
      </footer>
    </div>
    <!-- Global Mailform Output-->
    <div id="form-output-global" class="snackbars"></div>
    <!-- PhotoSwipe Gallery-->
    <div tabindex="-1" role="dialog" aria-hidden="true" class="pswp">
      <div class="pswp__bg"></div>
      <div class="pswp__scroll-wrap">
        <div class="pswp__container">
          <div class="pswp__item"></div>
          <div class="pswp__item"></div>
          <div class="pswp__item"></div>
        </div>
        <div class="pswp__ui pswp__ui--hidden">
          <div class="pswp__top-bar">
            <div class="pswp__counter"></div>
            <button title="Close (Esc)" class="pswp__button pswp__button--close"></button>
            <button title="Share" class="pswp__button pswp__button--share"></button>
            <button title="Toggle fullscreen" class="pswp__button pswp__button--fs"></button>
            <button title="Zoom in/out" class="pswp__button pswp__button--zoom"></button>
            <div class="pswp__preloader">
              <div class="pswp__preloader__icn">
                <div class="pswp__preloader__cut">
                  <div class="pswp__preloader__donut"></div>
                </div>
              </div>
            </div>
          </div>
          <div class="pswp__share-modal pswp__share-modal--hidden pswp__single-tap">
            <div class="pswp__share-tooltip"></div>
          </div>
          <button title="Previous (arrow left)" class="pswp__button pswp__button--arrow--left"></button>
          <button title="Next (arrow right)" class="pswp__button pswp__button--arrow--right"></button>
          <div class="pswp__caption">
            <div class="pswp__caption__center"></div>
          </div>
        </div>
      </div>
    </div>
    <!-- Java script-->
    <script src="js/core.min.js"> </script>
    <script src="js/script.js"></script>
    <!-- begin olark code-->
    <script data-cfasync="false" type="text/javascript">
      window.olark || (function (c) {
        var f = window, d = document, l = f.location.protocol == "https:" ? "https:" : "http:", z = c.name, r = "load";
        var nt = function () {
          f[z] = function () {
            (a.s = a.s || []).push(arguments)
          };
          var a = f[z]._ = {}, q = c.methods.length;
          while (q--) {
            (function (n) {
              f[z][n] = function () {
                f[z]("call", n, arguments)
              }
            })(c.methods[q])
          }
          a.l = c.loader;
          a.i = nt;
          a.p = {0: +new Date};
          a.P = function (u) {
            a.p[u] = new Date - a.p[0]
          };
          function s() {
            a.P(r);
            f[z](r)
          }
      
          f.addEventListener ? f.addEventListener(r, s, false) : f.attachEvent("on" + r, s);
          var ld = function () {
            function p(hd) {
              hd = "head";
              return ["<", hd, "></", hd, "><", i, ' onl' + 'oad="var d=', g, ";d.getElementsByTagName('head')[0].", j, "(d.", h, "('script')).", k, "='", l, "//", a.l, "'", '"', "></", i, ">"].join("")
            }
      
            var i = "body", m = d[i];
            if (!m) {
              return setTimeout(ld, 100)
            }
            a.P(1);
            var j = "appendChild", h = "createElement", k = "src", n = d[h]("div"), v = n[j](d[h](z)), b = d[h]("iframe"), g = "document", e = "domain", o;
            n.style.display = "none";
            m.insertBefore(n, m.firstChild).id = z;
            b.frameBorder = "0";
            b.id = z + "-loader";
            if (/MSIE[ ]+6/.test(navigator.userAgent)) {
              b.src = "javascript:false"
            }
            b.allowTransparency = "true";
            v[j](b);
            try {
              b.contentWindow[g].open()
            } catch (w) {
              c[e] = d[e];
              o = "javascript:var d=" + g + ".open();d.domain='" + d.domain + "';";
              b[k] = o + "void(0);"
            }
            try {
              var t = b.contentWindow[g];
              t.write(p());
              t.close()
            } catch (x) {
              b[k] = o + 'd.write("' + p().replace(/"/g, String.fromCharCode(92) + '"') + '");d.close();'
            }
            a.P(2)
          };
          ld()
        };
        nt()
      })({
        loader: "js/loader0.js",
        name: "olark",
        methods: ["configure", "extend", "declare", "identify"]
      });/* custom configuration goes here (www.olark.com/documentation) */olark.identify('7830-582-10-3714');
    </script>
    <noscript><a href="https://www.olark.com/site/7830-582-10-3714/contact" title="Contact us" target="_blank">Questions? Feedback?</a> powered by<a href="http://www.olark.com?welcome" title="Olark live chat software">Olark live chat software</a></noscript>
    <!-- end olark code-->
    
    
    
    <script type="text/javascript">
 var _gaq = _gaq || [];
  _gaq.push(['_setAccount', 'UA-7078796-5']);
  _gaq.push(['_trackPageview']);
  (function() {
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
  })();</script>
  </body><!-- Google Tag Manager --><noscript><iframe src="//www.googletagmanager.com/ns.html?id=GTM-P9FT69"height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript><script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start': new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src='//www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);})(window,document,'script','dataLayer','GTM-P9FT69');</script><!-- End Google Tag Manager -->
</html>