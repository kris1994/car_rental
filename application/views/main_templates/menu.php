<!-- Page-->
<div class="page text-center">
<header class="page-head header-panel-absolute bg-gray-darkest slider-menu-position">
        <!-- RD Navbar Transparent-->
        <div class="rd-navbar-wrap">
          <nav data-md-device-layout="rd-navbar-static" data-lg-device-layout="rd-navbar-static" data-stick-up-offset="120" class="rd-navbar rd-navbar-default" data-lg-auto-height="true" data-auto-height="false" data-md-layout="rd-navbar-static" data-lg-layout="rd-navbar-static" data-lg-stick-up="true">
            <div class="rd-navbar-inner">
              <!-- RD Navbar Panel-->
              <div class="rd-navbar-panel">
                <!-- RD Navbar Toggle-->
                <button data-rd-navbar-toggle=".rd-navbar, .rd-navbar-nav-wrap" class="rd-navbar-toggle"><span></span></button>
                <div class="rd-navbar-panel-title veil-md reveal-inline-block">
                  <h4>Home</h4>
                </div>
                <!-- RD Navbar Right Side Toggle-->
                <button data-rd-navbar-toggle=".right-side" class="rd-navbar-right-side-toggle veil-md"><span></span></button>
                <div class="shell">
                  <div class="range range-md-middle">
                    <div class="cell-md-3 left-side">
                      <div class="clearfix text-md-left text-center">
                        <!--Navbar Brand-->
                        <div class="rd-navbar-brand"><a href="index.html"><img width='229' height='48' src='images/logo-light-229x48.png' alt=''/></a></div>
                      </div>
                    </div>
                    <div class="cell-md-9 text-md-right right-side">
                      <ul class="list-unstyled">
                        <li class="reveal-md-inline-block"><span class="icon icon-xxs mdi mdi-email text-middle"></span><a href="mailto:#" class="inset-left-10 text-middle text-light">info@demolink.org</a></li>
                        <li class="reveal-md-inline-block"><span class="icon icon-xxs mdi mdi-clock text-middle"></span><span class="inset-left-10 text-middle text-light">Mon–Sat: 7:00–19:00</span></li>
                        <li class="reveal-md-inline-block"><span class="icon icon-xxs mdi mdi-phone text-middle"></span><a href="callto:#" class="inset-left-10 text-middle text-light">1-800-1234-567</a></li>
                      </ul>
                    </div>
                  </div>
                </div>
              </div>
              <div class="rd-navbar-menu-wrap">
                <div class="shell">
                  <div class="rd-navbar-nav-wrap">
                    <div class="rd-navbar-mobile-scroll">
                      <div class="rd-navbar-mobile-header-wrap">
                        <!--Navbar Brand Mobile-->
                        <div class="rd-navbar-mobile-brand"><a href="index.html"><img width='229' height='48' src='images/logo-light-229x48.png' alt=''/></a><a data-custom-toggle="rd-navbar-search-mobile" href="#" class="rd-navbar-mobile-search-toggle mdi"><span></span></a></div>
                        <!--RD Navbar Mobile Search-->
                        <div id="rd-navbar-search-mobile" class="rd-navbar-search-mobile">
                          <form action="search-results.html" method="GET" class="rd-navbar-search-form search-form-icon-right rd-search">
                            <div class="form-group">
                              <label for="rd-navbar-mobile-search-form-input" class="form-label">Search</label>
                              <input id="rd-navbar-mobile-search-form-input" type="text" name="s" autocomplete="off" class="rd-navbar-search-form-input form-control form-control-gray-lightest"/>
                            </div>
                          </form>
                        </div>
                      </div>
                      <!-- RD Navbar Nav-->
                      <ul class="rd-navbar-nav">
                        <li class=" active"><a href="index.html">Home</a>
                        </li>
                        <li><a href="gallery-masonry.html">Vehicle Gallery</a>
                          <ul class="rd-navbar-dropdown">
                            <li><a href="gallery-masonry.html">Masonry Grid</a>
                            </li>
                            <li><a href="gallery-grid-3-columns.html">Grid With Padding</a>
                            </li>
                            <li><a href="gallery-grid-3-columns-fullwidth.html">Grid With No Padding</a>
                            </li>
                            <li><a href="gallery-cobbles.html">Cobbles Grid</a>
                            </li>
                          </ul>
                        </li>
                        <li class=" tabs-nav"><a href="services.html">Services</a>
                          <ul class="rd-navbar-dropdown">
                            <li><a href="services.html#0">History Check</a>
                            </li>
                            <li><a href="services.html#1">Buyers Guide</a>
                            </li>
                            <li><a href="services.html#2">Car Check</a>
                            </li>
                            <li><a href="services.html#3">Car Insurance</a>
                            </li>
                            <li><a href="services.html#4">Customer Support</a>
                            </li>
                            <li><a href="services.html#5">Warranty Programs</a>
                            </li>
                          </ul>
                        </li>
                        <li><a href="#">Pages</a>
                          <div class="rd-navbar-megamenu">
                            <div class="row section-relative">
                              <div class="col-lg-3 veil reveal-lg-block">
                                <div>
                                  <div>
                                    <p style="min-height:300px"></p>
                                  </div>
                                </div>
                              </div>
                              <ul class="col-lg-3 col-md-4">
                                <li>
                                  <h6 class="text-bold text-white">Pages</h6>
                                  <ul class="list-unstyled offset-lg-top-25">
                                    <li><a href="about-us.html">About Us</a></li>
                                    <li><a href="prices.html">Prices</a></li>
                                    <li><a href="calculate.html">Calculate</a></li>
                                    <li><a href="team.html">Our Team</a></li>
                                    <li><a href="services-1.html">Services 1</a></li>
                                    <li><a href="services-2.html">Services 2</a></li>
                                    <li><a href="team-member.html">Team Member Profile</a></li>
                                    <li><a href="404.html">404</a></li>
                                    <li><a href="maintenance.html">Maintenance</a></li>
                                    <li><a href="comming-soon.html">Coming Soon</a></li>
                                    <li><a href="faq.html">FAQs</a></li>
                                    <li><a href="clients.html">Clients</a></li>
                                  </ul>
                                </li>
                              </ul>
                              <ul class="col-lg-3 col-md-4">
                                <li>
                                  <h6 class="text-bold text-white">Additional Pages</h6>
                                  <ul class="list-unstyled offset-lg-top-25">
                                    <li><a href="tabs-and-accordions.html">Tabs &amp; Accordions</a></li>
                                    <li><a href="typography.html">Typography</a></li>
                                    <li><a href="forms.html">Forms</a></li>
                                    <li><a href="buttons.html">Buttons</a></li>
                                    <li><a href="grid.html">Grid</a></li>
                                    <li><a href="icons.html">Icons</a></li>
                                    <li><a href="tables.html">Tables</a></li>
                                    <li><a href="progress-bars.html">Progress bars</a></li>
                                  </ul>
                                </li>
                              </ul>
                              <ul class="col-lg-3 col-md-4">
                                <li>
                                  <h6 class="text-bold text-white">Elements</h6>
                                  <ul class="list-unstyled offset-lg-top-25">
                                    <li><a href="header-transparent.html">Header Transparent</a></li>
                                    <li><a href="header-center-footer-light.html">Header Center, Footer Light</a></li>
                                    <li><a href="header-minimal-footer-corporate.html">Header Minimal, Footer Corporate</a></li>
                                    <li><a href="header-corporate.html">Header Corporate</a></li>
                                    <li><a href="header-hamburger-menu.html">Header Hamburger Menu</a></li>
                                    <li><a href="footer-center-dark.html">Footer Center Dark</a></li>
                                    <li><a href="footer-center-light.html">Footer Center Light</a></li>
                                    <li><a href="footer-minimal-dark.html">Footer Minimal Dark</a></li>
                                    <li><a href="footer-widget-light.html">Footer Widget Light</a></li>
                                    <li><a href="footer-widget-dark.html">Footer Widget Dark</a></li>
                                  </ul>
                                </li>
                              </ul>
                            </div>
                          </div>
                        </li>
                        <li><a href="blog-default.html">Blog</a>
                          <ul class="rd-navbar-dropdown">
                            <li><a href="blog-archive.html">Archive Page</a>
                            </li>
                            <li><a href="blog-2-columns-layout.html">2 Columns Layout</a>
                            </li>
                            <li><a href="blog-3-column-layout.html">3 Columns Grid Layout</a>
                            </li>
                            <li><a href="blog-default.html">Default Blog</a>
                            </li>
                            <li><a href="blog-left-sidebar.html">Left Sidebar</a>
                            </li>
                            <li><a href="blog-right-sidebar.html">Right Sidebar</a>
                            </li>
                            <li><a href="blog-timeline.html">Timeline</a>
                            </li>
                            <li><a href="blog-post-page.html">Post Page</a>
                            </li>
                          </ul>
                        </li>
                        <li><a href="contacts.html">Contacts</a>
                          <ul class="rd-navbar-dropdown">
                            <li><a href="contacts.html">Contacts</a>
                            </li>
                            <li><a href="contacts-variant-2.html">Contacts 2</a>
                            </li>
                          </ul>
                        </li>
                      </ul>
                    </div>
                  </div>
                  <!--RD Navbar Search-->
                  <div class="rd-navbar-search"><a data-rd-navbar-toggle=".rd-navbar-search" href="#" class="rd-navbar-search-toggle mdi"><span></span></a>
                    <form action="search-results.html" data-search-live="rd-search-results-live" method="GET" class="rd-navbar-search-form search-form-icon-right rd-search">
                      <div class="form-group">
                        <label for="rd-navbar-search-form-input" class="form-label">Search</label>
                        <input id="rd-navbar-search-form-input" type="text" name="s" autocomplete="off" class="rd-navbar-search-form-input form-control form-control-gray-lightest"/>
                        <div id="rd-search-results-live" class="rd-search-results-live"></div>
                      </div>
                    </form>
                  </div>
                </div>
              </div>
            </div>
          </nav>
        </div>
      </header>