<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class MY_Controller extends CI_Controller 
{
    public function __construct() {
        parent::__construct();  
        $this->load->library("session");      
    }    
    public function load_main_html($url, $html_data = null)
    {  
       
            $data['html_title'] = 'Car Rental';
            $data['html_header']= $this->load->view('main_templates/header', null, true);
            $data['html_menu']= $this->load->view('main_templates/menu',null, true);
            $data['html_body']  = $this->load->view($url, $html_data, true);
            $data['html_footer']= $this->load->view('main_templates/footer', null, true);
       
            $this->load->view('main_templates/index', $data );
            
           
    }
    
}
/* End of file MY_Controller.php */
/* Location: ./application/core/MY_Controller.php */