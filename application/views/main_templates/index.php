<!DOCTYPE html>
<html lang="en" class="wide wow-animation smoothscroll scrollTo">
  <head>
    <!-- Site Title-->
    <title><?php echo $html_title?></title>
    <meta charset="utf-8">
    <meta name="format-detection" content="telephone=no">
    <meta name="viewport" content="width=device-width, height=device-height, initial-scale=1.0, maximum-scale=1.0, user-scalable=0">
    <meta http-equiv="X-UA-Compatible" content="IE=Edge">
    <meta name="keywords" content="intense web design multipurpose template">
    <meta name="date" content="Dec 26">
    <?php echo $html_header?>
  </head>
  <body>
  	<div id="olark" style="display: none;"><olark><iframe frameborder="0" id="olark-loader"></iframe></olark></div>

      <!-- Page Header-->
      <?php echo $html_menu ?>
      <!-- Page Content-->
      <main class="page-content">
        <section class="context-dark bg-dark-blue">
          <!-- Swiper-->
          <div data-height="100vh" data-loop="true" data-dragable="false" data-min-height="480px" data-slide-effect="true" class="swiper-container swiper-slider">
            <div class="swiper-wrapper">
              <div data-slide-bg="images/services/bg-01-1920x654.jpg" style="background-position: center center" class="swiper-slide">
                <div class="swiper-slide-caption section-md-top-185">
                  <div class="container">
                    <div class="range range-xs-center range-lg-left">
                      <div class="cell-lg-7 text-lg-left cell-xs-10">
                        <div data-caption-animate="fadeInUp" data-caption-delay="100">
                          <h1>New and Used Cars</h1>
                        </div>
                        <div data-caption-animate="fadeInUp" data-caption-delay="150" class="offset-top-15 offset-xl-top-30">
                          <h4 class="font-default text-light text-spacing-20">Explore the vast model range of new and used cars by widely known manufacturers on our website.</h4>
                        </div>
                        <div data-caption-animate="fadeInUp" data-caption-delay="200" class="offset-top-20"><a href="about-us.html" class="btn btn-default btn-sm">learn more</a></div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <div data-slide-bg="images/services/bg-03-1920x766.jpg" style="background-position: center center" class="swiper-slide">
                <div class="swiper-slide-caption section-md-top-185">
                  <div class="container">
                    <div class="range range-xs-center range-lg-left">
                      <div class="cell-lg-7 text-lg-left cell-xs-10">
                        <div data-caption-animate="fadeInUp" data-caption-delay="100">
                          <h1>Latest Car Reviews</h1>
                        </div>
                        <div data-caption-animate="fadeInUp" data-caption-delay="150" class="offset-top-15 offset-xl-top-30">
                          <h4 class="font-default text-light text-spacing-20">Read the latest car reviews written by our consultants or submit your own car review to our website's blog!</h4>
                        </div>
                        <div data-caption-animate="fadeInUp" data-caption-delay="200" class="offset-top-20"><a href="about-us.html" class="btn btn-default btn-sm">learn more</a></div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <div data-slide-bg="images/services/bg-04-1920x766.jpg" style="background-position: center center" class="swiper-slide">
                <div class="swiper-slide-caption section-md-top-185">
                  <div class="container">
                    <div class="range range-xs-center range-lg-left">
                      <div class="cell-lg-7 text-lg-left cell-xs-10">
                        <div data-caption-animate="fadeInUp" data-caption-delay="200">
                          <h1>Locate a Car Dealer</h1>
                        </div>
                        <div data-caption-animate="fadeInUp" data-caption-delay="300" class="offset-top-15 offset-xl-top-30">
                          <h4 class="font-default text-light text-spacing-20">We can help you find an appropriate car dealership according to your preferences and location.</h4>
                        </div>
                        <div data-caption-animate="fadeInUp" data-caption-delay="500" class="offset-top-20"><a href="about-us.html" class="btn btn-default btn-sm"> learn more</a></div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <!-- Swiper Pagination-->
            <div class="swiper-pagination"> </div>
          </div>
        </section>
        <section class="section-70 section-md-90">
          <div class="shell text-sm-left">
            <h2>Car search</h2>
            <hr class="divider divider-lg bg-primary hr-sm-left-0">
            <div class="offset-top-50">
              <p>Please complete and submit this form and one of our representatives will reply promptly. Thank You!</p>
            </div>
            <div class="offset-top-20">
              <form method="post" action="bat/rd-mailform.php" data-form-output="form-output-global" class="rd-mailform text-left">
                <div class="range range-xs-bottom">
                  <div class="cell-sm-6 cell-lg-4">
                    <div class="form-group">
                      <label for="form-car-search-make" class="form-label form-label-outside">Make:</label>
                      <select id="form-car-search-make" name="make" class="form-control">
                        <option value="1">Enter car make...</option>
                        <option value="2">Audi</option>
                        <option value="3">Acura</option>
                        <option value="4">BMW</option>
                        <option value="5">Chevrolet</option>
                      </select>
                    </div>
                  </div>
                  <div class="cell-sm-6 cell-lg-4 offset-top-30 offset-sm-top-0">
                    <div class="form-group">
                      <label for="form-car-search-model" class="form-label form-label-outside">Model:</label>
                      <select id="form-car-search-model" name="model" class="form-control">
                        <option value="1">Enter model</option>
                        <option value="2">x2 series</option>
                        <option value="3">x3 series</option>
                        <option value="4">x4 series</option>
                        <option value="5">x5 series</option>
                      </select>
                    </div>
                  </div>
                  <div class="cell-sm-6 cell-lg-4 offset-top-30">
                    <div class="form-group">
                      <label for="form-car-search-country" class="form-label form-label-outside">Country:</label>
                      <select id="form-car-search-country" name="country" class="form-control">
                        <option value="1">Country</option>
                        <option value="2">USA</option>
                        <option value="3">Germany</option>
                        <option value="4">Italy</option>
                        <option value="5">Japan</option>
                      </select>
                    </div>
                  </div>
                  <div class="cell-sm-6 cell-lg-4 offset-top-30">
                    <div class="form-group">
                      <label for="form-car-search-city" class="form-label form-label-outside">City</label>
                      <select id="form-car-search-city" name="city" class="form-control">
                        <option value="1">City name</option>
                        <option value="2">Tokio</option>
                        <option value="3">Munchen</option>
                        <option value="4">New York</option>
                        <option value="5">Milan</option>
                      </select>
                    </div>
                  </div>
                  <div class="cell-sm-3 cell-lg-4 offset-top-30">
                    <div class="form-group">
                      <label for="form-car-search-body-type" class="form-label form-label-outside">Body Type:</label>
                      <select id="form-car-search-body-type" name="body-type" class="form-control">
                        <option value="1">Enter</option>
                        <option value="2">Hatchback</option>
                        <option value="3">Mini-MPVs</option>
                        <option value="4">Crossovers</option>
                        <option value="5">Coupes</option>
                      </select>
                    </div>
                  </div>
                  <div class="cell-sm-3 cell-lg-2 offset-top-30">
                    <div class="form-group">
                      <label for="form-car-search-min-year" class="form-label form-label-outside"> Min Year:</label>
                      <input id="form-car-search-min-year" type="text" name="min-year" placeholder="0" data-constraints="@Required @IsNumeric" class="form-control">
                    </div>
                  </div>
                  <div class="cell-sm-3 cell-lg-2 offset-top-30">
                    <div class="form-group">
                      <label for="form-car-search-max-year" class="form-label form-label-outside"> Max Year:</label>
                      <input id="form-car-search-max-year" type="text" name="max-year" placeholder="0" data-constraints="@Required @IsNumeric" class="form-control">
                    </div>
                  </div>
                  <div class="cell-sm-3 cell-lg-3 offset-top-30">
                    <div class="form-group">
                      <label for="form-quote-color" class="form-label form-label-outside">Color</label>
                      <select id="form-quote-color" name="color" class="form-control">
                        <option value="1">No</option>
                        <option value="2">Black</option>
                        <option value="3">White</option>
                        <option value="4">Red</option>
                        <option value="5">Blue</option>
                      </select>
                    </div>
                  </div>
                  <div class="cell-sm-3 cell-lg-2 offset-top-30">
                    <div class="form-group">
                      <label for="form-quote-height" class="form-label form-label-outside"> Min Price:</label>
                      <input id="form-quote-height" type="text" name="height" placeholder="0" data-constraints="@Required @IsNumeric" class="form-control">
                    </div>
                  </div>
                  <div class="cell-sm-3 cell-lg-2 offset-top-30">
                    <div class="form-group">
                      <label for="form-quote-width" class="form-label form-label-outside"> Max Price:</label>
                      <input id="form-quote-width" type="text" name="width" placeholder="1" data-constraints="@Required @IsNumeric" class="form-control">
                    </div>
                  </div>
                  <div class="cell-sm-3 cell-lg-2 offset-top-30"><a href="calculate.html" class="btn btn-primary btn-sm">calculate</a></div>
                </div>
              </form>
            </div>
          </div>
        </section>
        <!-- counters-->
        <section class="bg-dark-blue context-dark">
          <!-- RD Parallax-->
          <div data-on="false" data-md-on="true" class="rd-parallax">
            <div data-speed="0.35" data-type="media" data-url="images/services/bg-04-1920x500.jpg" class="rd-parallax-layer"></div>
            <div data-speed="0" data-type="html" class="rd-parallax-layer">
              <div class="shell text-sm-left section-65 section-sm-95">
                <h2>counters</h2>
                <hr class="divider divider-lg bg-primary hr-sm-left-0">
                <div class="range range-xs-center range-md-left offset-top-50 text-center counters">
                  <div class="cell-sm-6 cell-md-3">
                    <!-- Counter type 1-->
                    <div class="counter-type-1">
                      <div>
                        <div class="h2 font-accent text-regular"><span data-step="3000" data-from="0" data-to="1324" class="counter"></span></div>
                      </div>
                      <div class="offset-top-12">
                        <h6 class="text-primary text-regular">Types of models</h6>
                      </div>
                    </div>
                  </div>
                  <div class="cell-sm-6 cell-md-3 offset-top-65 offset-sm-top-0">
                    <!-- Counter type 1-->
                    <div class="counter-type-1">
                      <div>
                        <div class="h2 font-accent text-regular"><span data-step="2500" data-from="0" data-to="65" class="counter"></span></div>
                      </div>
                      <div class="offset-top-12">
                        <h6 class="text-primary text-regular">Certified consultants</h6>
                      </div>
                    </div>
                  </div>
                  <div class="cell-sm-6 cell-md-3 offset-top-65 offset-md-top-0">
                    <!-- Counter type 1-->
                    <div class="counter-type-1">
                      <div>
                        <div class="h2 font-accent text-regular"><span data-step="1500" data-from="0" data-to="268" class="counter"></span></div>
                      </div>
                      <div class="offset-top-12">
                        <h6 class="text-primary text-regular">Models we sell</h6>
                      </div>
                    </div>
                  </div>
                  <div class="cell-sm-6 cell-md-3 offset-top-65 offset-md-top-0">
                    <!-- Counter type 1-->
                    <div class="counter-type-1">
                      <div>
                        <div class="h2 font-accent text-regular"><span data-step="5300" data-from="0" data-to="2847951" class="counter"></span></div>
                      </div>
                      <div class="offset-top-12">
                        <h6 class="text-primary text-regular">Happy clients</h6>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </section>
        <!-- Classic Thumbnail-->
        <section class="section-80 section-lg-90 bg-lightest">
          <div class="shell text-sm-left">
            <h2>our services</h2>
            <hr class="divider divider-lg bg-primary hr-sm-left-0">
            <div class="range range-xs-center range-sm-left offset-top-50">
              <div class="cell-xs-10 cell-sm-8">
                <p>We strive to provide our customers with the best services possible.</p>
              </div>
            </div>
            <div class="range text-left range-xs-center offset-top-50">
              <div class="cell-xs-8 cell-sm-6 cell-md-4"><a class="thumbnail-classic" href="services.html" target="_self">
                          <figure><img width="370" height="250" src="images/services/service-01-370x250.jpg" alt="">
                            <figcaption class="thumbnail-classic-caption"><span class="icon icon-xxs fa-arrow-right"></span>
                              <h6 class="thumbnail-classic-title offset-top-0 text-uppercase">History Check</h6>
                            </figcaption>
                          </figure></a>
              </div>
              <div class="cell-xs-8 cell-sm-6 cell-md-4 offset-top-30 offset-sm-top-0"><a class="thumbnail-classic" href="services.html#undefined2" target="_self">
                          <figure><img width="370" height="250" src="images/services/service-02-370x250.jpg" alt="">
                            <figcaption class="thumbnail-classic-caption"><span class="icon icon-xxs fa-arrow-right"></span>
                              <h6 class="thumbnail-classic-title offset-top-0 text-uppercase">Buyers Guide</h6>
                            </figcaption>
                          </figure></a>
              </div>
              <div class="cell-xs-8 cell-sm-6 cell-md-4 offset-top-30 offset-md-top-0"><a class="thumbnail-classic" href="services.html#undefined3" target="_self">
                          <figure><img width="370" height="250" src="images/services/service-03-370x250.jpg" alt="">
                            <figcaption class="thumbnail-classic-caption"><span class="icon icon-xxs fa-arrow-right"></span>
                              <h6 class="thumbnail-classic-title offset-top-0 text-uppercase">Car Insurance</h6>
                            </figcaption>
                          </figure></a>
              </div>
              <div class="cell-xs-8 cell-sm-6 cell-md-4 offset-top-30"><a class="thumbnail-classic" href="services.html#undefined4" target="_self">
                          <figure><img width="370" height="250" src="images/services/service-04-370x250.jpg" alt="">
                            <figcaption class="thumbnail-classic-caption"><span class="icon icon-xxs fa-arrow-right"></span>
                              <h6 class="thumbnail-classic-title offset-top-0 text-uppercase">Car Check</h6>
                            </figcaption>
                          </figure></a>
              </div>
              <div class="cell-xs-8 cell-sm-6 cell-md-4 offset-top-30"><a class="thumbnail-classic" href="services.html#undefined5" target="_self">
                          <figure><img width="370" height="250" src="images/services/service-05-370x250.jpg" alt="">
                            <figcaption class="thumbnail-classic-caption"><span class="icon icon-xxs fa-arrow-right"></span>
                              <h6 class="thumbnail-classic-title offset-top-0 text-uppercase">Warranty Programs</h6>
                            </figcaption>
                          </figure></a>
              </div>
              <div class="cell-xs-8 cell-sm-6 cell-md-4 offset-top-30"><a class="thumbnail-classic" href="services.html#undefined6" target="_self">
                          <figure><img width="370" height="250" src="images/services/service-06-370x250.jpg" alt="">
                            <figcaption class="thumbnail-classic-caption"><span class="icon icon-xxs fa-arrow-right"></span>
                              <h6 class="thumbnail-classic-title offset-top-0 text-uppercase">Customer Support</h6>
                            </figcaption>
                          </figure></a>
              </div>
            </div>
            <div class="offset-top-50"><a href="services-1.html" class="btn btn-default btn-sm">view all services</a></div>
          </div>
        </section>
        <section class="section-80">
          <div class="shell text-sm-left">
            <h2>Professional team</h2>
            <hr class="divider divider-lg bg-primary hr-sm-left-0">
            <div class="range range-xs-center range-sm-left offset-top-50">
              <div class="cell-xs-10 cell-sm-6 cell-md-3"><img src="images/services/user-alan-smith-270x270.jpg" width="270" height="270" alt="" class="img-responsive reveal-inline-block">
                <div class="offset-top-20">
                  <h5 class="text-primary text-regular"><a href="team-member.html">Alan Smith</a></h5>
                </div>
                <p class="offset-top-5 text-silver text-light">Owner/Partner</p>
                <p>Alan’s rich experience includes car sales, rental car management, and service advising.</p>
                <div class="offset-top-20">
                  <ul class="list-inline list-inline-dark list-inline-sm">
                    <li><a href="#" class="icon icon-xxs fa-facebook text-middle"></a></li>
                    <li><a href="#" class="icon icon-xxs fa-twitter text-middle"></a></li>
                    <li><a href="#" class="icon icon-xxs fa-google text-middle"></a></li>
                  </ul>
                </div>
              </div>
              <div class="cell-xs-10 cell-sm-6 cell-md-3 offset-top-50 offset-sm-top-0"><img src="images/services/user-laura-stegner-270x270.jpg" width="270" height="270" alt="" class="img-responsive reveal-inline-block">
                <div class="offset-top-20">
                  <h5 class="text-primary text-regular"><a href="team-member.html">Laura Stegner</a></h5>
                </div>
                <p class="offset-top-5 text-silver text-light">Sales Manager</p>
                <p>Her goal is to help you make a wise investment decision regarding your needs.</p>
                <div class="offset-top-20">
                  <ul class="list-inline list-inline-dark list-inline-sm">
                    <li><a href="#" class="icon icon-xxs fa-facebook text-middle"></a></li>
                    <li><a href="#" class="icon icon-xxs fa-twitter text-middle"></a></li>
                    <li><a href="#" class="icon icon-xxs fa-google text-middle"></a></li>
                  </ul>
                </div>
              </div>
              <div class="cell-xs-10 cell-sm-6 cell-md-3 offset-top-50 offset-md-top-0"><img src="images/services/user-john-franklin-270x270.jpg" width="270" height="270" alt="" class="img-responsive reveal-inline-block">
                <div class="offset-top-20">
                  <h5 class="text-primary text-regular"><a href="team-member.html">John Franklin</a></h5>
                </div>
                <p class="offset-top-5 text-silver text-light">Certified Technician</p>
                <p>Everyone enjoys his expertise as a technician and a great personality around the shop.</p>
                <div class="offset-top-20">
                  <ul class="list-inline list-inline-dark list-inline-sm">
                    <li><a href="#" class="icon icon-xxs fa-facebook text-middle"></a></li>
                    <li><a href="#" class="icon icon-xxs fa-twitter text-middle"></a></li>
                    <li><a href="#" class="icon icon-xxs fa-google text-middle"></a></li>
                  </ul>
                </div>
              </div>
              <div class="cell-xs-10 cell-sm-6 cell-md-3 offset-top-50 offset-md-top-0"><img src="images/services/user-martha-healy-270x270.jpg" width="270" height="270" alt="" class="img-responsive reveal-inline-block">
                <div class="offset-top-20">
                  <h5 class="text-primary text-regular"><a href="team-member.html">Martha Healy</a></h5>
                </div>
                <p class="offset-top-5 text-silver text-light">Service Advisor</p>
                <p>Martha is a leader in providing each of our customers with the excellent customer service.</p>
                <div class="offset-top-20">
                  <ul class="list-inline list-inline-dark list-inline-sm">
                    <li><a href="#" class="icon icon-xxs fa-facebook text-middle"></a></li>
                    <li><a href="#" class="icon icon-xxs fa-twitter text-middle"></a></li>
                    <li><a href="#" class="icon icon-xxs fa-google text-middle"></a></li>
                  </ul>
                </div>
              </div>
            </div>
          </div>
        </section>
        <!-- Testimonials-->
        <section class="bg-dark-blue context-dark">
          <!-- RD Parallax-->
          <div data-on="false" data-md-on="true" class="rd-parallax">
            <div data-speed="0.35" data-type="media" data-url="images/services/bg-05-1920x609.jpg" class="rd-parallax-layer"></div>
            <div data-speed="0" data-type="html" class="rd-parallax-layer">
              <div class="shell text-md-left section-90 section-md-bottom-100">
                <h2>Testimonials</h2>
                <hr class="divider divider-lg bg-primary hr-md-left-0">
                <div class="offset-top-50">
                  <div data-loop="false" data-items="1" data-dots="true" data-mouse-drag="false" data-sm-items="2" data-nav="false" data-margin="30" class="owl-carousel owl-carousel-default">
                    <div>
                      <div class="quote">
                        <div class="unit unit-md unit-md-horizontal text-md-left">
                          <div class="unit-left"><img src="images/services/user-evelyn-fisher-140x140.jpg" width="140" height="140" alt="" class="img-responsive reveal-inline-block img-circle"></div>
                          <div class="unit-body">
                            <div class="inset-md-right-40">
                              <p>
                                <q>I had a great experience at this car dealership. Eva was quick, and she helped us find the car and price we hoped for. We had a hard time finding car we wanted but you had a perfect match for us!</q>
                              </p>
                              <p>
                                <cite>Evelyn Fisher</cite>
                              </p>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                    <div>
                      <div class="quote">
                        <div class="unit unit-md unit-md-horizontal text-md-left">
                          <div class="unit-left"><img src="images/services/user-lawrence-sims-140x140.jpg" width="140" height="140" alt="" class="img-responsive reveal-inline-block img-circle"></div>
                          <div class="unit-body">
                            <div class="inset-md-right-40">
                              <p>
                                <q>I have never thought purchasing a car can be so easy! You not just offer wide assortment of cars and vehicles, you provide pleasant customer service, and it’s what I like most!</q>
                              </p>
                              <p>
                                <cite>Lawrence Sims</cite>
                              </p>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                    <div>
                      <div class="quote">
                        <div class="unit unit-md unit-md-horizontal text-md-left">
                          <div class="unit-left"><img src="images/services/user-jenny-myers-140x140.jpg" width="140" height="140" alt="" class="img-responsive reveal-inline-block img-circle"></div>
                          <div class="unit-body">
                            <div class="inset-md-right-40">
                              <p>
                                <q>These guys are true professionals. Not only they gave me a great value for my old car but also helped me find an amazing new vehicle. They have a very nice selection of  affordable vehicles.</q>
                              </p>
                              <p>
                                <cite>Cristal Smith</cite>
                              </p>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                    <div>
                      <div class="quote">
                        <div class="unit unit-md unit-md-horizontal text-md-left">
                          <div class="unit-left"><img src="images/services/user-peter-johnson-140x140.jpg" width="140" height="140" alt="" class="img-responsive reveal-inline-block img-circle"></div>
                          <div class="unit-body">
                            <div class="inset-md-right-40">
                              <p>
                                <q>I had a great experience at this car dealership. Eva was quick, and she helped us find the car and price we hoped for. We had a hard time finding car we wanted but you had a perfect match for us!</q>
                              </p>
                              <p>
                                <cite>Patrick Pool</cite>
                              </p>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                    <div>
                      <div class="quote">
                        <div class="unit unit-md unit-md-horizontal text-md-left">
                          <div class="unit-left"><img src="images/services/user-lawrence-sims-140x140.jpg" width="140" height="140" alt="" class="img-responsive reveal-inline-block img-circle"></div>
                          <div class="unit-body">
                            <div class="inset-md-right-40">
                              <p>
                                <q>Frankly, I have bought a few vehicles from different dealerships, but AutoTraider has the most professional sales managers and they treat customers with respect. I will surely recommend them to my friends.</q>
                              </p>
                              <p>
                                <cite>Sam Kromstain</cite>
                              </p>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                    <div>
                      <div class="quote">
                        <div class="unit unit-md unit-md-horizontal text-md-left">
                          <div class="unit-left"><img src="images/services/user-evelyn-fisher-140x140.jpg" width="140" height="140" alt="" class="img-responsive reveal-inline-block img-circle"></div>
                          <div class="unit-body">
                            <div class="inset-md-right-40">
                              <p>
                                <q>My husband and I had a really good experience. Never felt pressured, haggled, or lied to. They gave us fair market value without having to negotiate. Also, this dealership is very nicely updated.</q>
                              </p>
                              <p>
                                <cite>Eva Pool</cite>
                              </p>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </section>
      </main>
      <!-- Page Footer-->
      <?php echo $html_footer; ?>
  </body><!-- Google Tag Manager --><noscript><iframe src="//www.googletagmanager.com/ns.html?id=GTM-P9FT69"height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript><script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start': new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src='//www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);})(window,document,'script','dataLayer','GTM-P9FT69');</script><!-- End Google Tag Manager -->
</html>